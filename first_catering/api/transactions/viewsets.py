# Project Imports
from common.viewsets import BaseCreateRetrieveUpdateViewSet, BaseParameteredCreateRetrieveUpdateViewSet

# App Imports
from .models import Transaction, TransactionPayment
from .serializers import TransactionSerializer, TransactionPaymentSerializer

class TransactionViewSet(BaseCreateRetrieveUpdateViewSet):
    """
    This viewset automatically provides `create`, `retrieve` and `update` actions.
    """
    lookup_field = 'uuid'

    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

class TransactionPaymentViewSet(BaseCreateRetrieveUpdateViewSet):
    """
    This viewset automatically provides `create`, `retrieve` and `update` actions.
    The `create` action for this endpoint will perform multiple tasks.
    """
    lookup_field = 'uuid'

    # parameter_lists = [
    #     [
    #         "cashless"
    #     ]
    # ]

    def construct_serializer_data(request_data, parameters):
        print(parameters)
        return request_data

    queryset = TransactionPayment.objects.all()
    serializer_class = TransactionPaymentSerializer
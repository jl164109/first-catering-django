from django.apps import AppConfig


class TransactionsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api.transactions'
    verbose_name = 'Transactions'

    def ready(self):
        from . import receivers
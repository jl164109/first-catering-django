# Project Imports
from common.serializers import BaseDateTimeSerializer, BaseUUIDSerializer

# App Imports
from .models import Transaction, TransactionPayment

class TransactionSerializer(BaseDateTimeSerializer, BaseUUIDSerializer):
    class Meta:
        model = Transaction
        fields = (
            'total_paid',
            'success',
            'void',
        ) + BaseDateTimeSerializer.Meta.fields + BaseUUIDSerializer.Meta.fields

class TransactionPaymentSerializer(BaseDateTimeSerializer, BaseUUIDSerializer):
    class Meta:
        model = TransactionPayment
        fields = (
            'transaction',
            'cashless_adjustment',
            'sub_paid',
            'success',
            'void',
        ) + BaseDateTimeSerializer.Meta.fields + BaseUUIDSerializer.Meta.fields
# Project Imports
from django.contrib import admin
from common.admin import BaseDateTimeAdmin

# App Imports
from .models import Transaction, TransactionPayment

# Transaction Admin Declaration - Extends BaseDateTime Admin
class TransactionAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Transaction Info', {
            'fields': [
                'total_paid',
                'success',
                'void',
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# TransactionPayment Admin Declaration - Extends BaseDateTime Admin
class TransactionPaymentAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Transaction Payment Info', {
            'fields': [
                'sub_paid',
                'success',
                'void',
            ],
        }),
        ('Transaction Payment Relationships', {
            'fields': [
                'cashless_adjustment',
                'transaction'
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# Register Transaction Admin to the Admin Dashboard
admin.site.register(Transaction, TransactionAdmin)

# Register TransactionPayment Admin to the Admin Dashboard
admin.site.register(TransactionPayment, TransactionPaymentAdmin)
# Project Imports
from django.db.models.signals import pre_save
from django.dispatch import receiver
from decimal import Decimal

# App Imports
from .models import TransactionPayment

@receiver(pre_save, sender=TransactionPayment)
def transaction_payment_pre_save(sender, instance, **kwargs):
    instance.pre_save = { 'sub_paid': Decimal(0.00), 'void': False }

    # Store the current value of the instance's sub_paid
    if(instance.id):
        instance.pre_save['sub_paid'] = sender.objects.get(id=instance.id).sub_paid
        instance.pre_save['void'] = sender.objects.get(id=instance.id).void

    # Check for a transaction payment void and exit after removing previous amount
    if((instance.pre_save['void'] != instance.void) and instance.void):
        transaction_payment_change(
            instance=instance,
            difference=(-instance.pre_save['sub_paid'])
        )
        return

    # Check for a change in the sub_paid and update total_paid
    if(instance.pre_save['sub_paid'] != instance.sub_paid):
        transaction_payment_change(
            instance=instance,
            difference=(instance.sub_paid - instance.pre_save['sub_paid'])
        )

def transaction_payment_change(instance, difference):
    # Add Difference and Save
    instance.transaction.total_paid = instance.transaction.total_paid + difference
    instance.transaction.save()

    # Logging
    print(
        'Transaction: {transaction} Total Paid has changed by + £{difference}'.format(
            transaction = instance.transaction,
            difference = difference
        ).replace('+ £-', '- £')
    )

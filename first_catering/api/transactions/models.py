# Project Imports
from common.models import BaseDateTimeModel, BaseUUIDModel
from django.db import models

# App Imports
from api.cashlesses.models import CashlessAdjustment

# Transaction Model Declaration - Extends BaseDateTime and Base UUID Models
class Transaction(BaseDateTimeModel, BaseUUIDModel):
    # Model Properties
    total_paid = models.DecimalField(verbose_name='Total Paid', max_digits=6, decimal_places=2, default=0.00)
    success = models.BooleanField(verbose_name='Success', default=False)
    void = models.BooleanField(verbose_name='Void', default=False)

    # Readabilty
    class Meta:
        verbose_name = 'Transaction'
        verbose_name_plural = 'Transactions'

    def __str__(self):
        return '{uuid}: £{total_paid}'.format(
            uuid = self.uuid,
            total_paid = self.total_paid,
        )

# TransactionPayment Model Declaration - Extends BaseDateTime and Base UUID Models
class TransactionPayment(BaseDateTimeModel, BaseUUIDModel):
    # Foreign Keys
    cashless_adjustment = models.OneToOneField(CashlessAdjustment, to_field='uuid', on_delete=models.SET_NULL, null=True)
    transaction = models.ForeignKey(Transaction, to_field='uuid', on_delete=models.SET_NULL, null=True)

    # Model Properties
    sub_paid = models.DecimalField(verbose_name='Sub Total Paid', max_digits=6, decimal_places=2, default=0.00)
    success = models.BooleanField(verbose_name='Success', default=False)
    void = models.BooleanField(verbose_name='Void', default=False)

    # Readabilty
    class Meta:
        verbose_name = 'Transaction Payment'
        verbose_name_plural = 'Transaction Payments'

    def __str__(self):
        return '{uuid}: £{sub_paid}{void}'.format(
            uuid = self.uuid,
            sub_paid = self.sub_paid,
            void = ' - VOID' if (self.void) else ''
        )

    # Save Hook
    def save(self, *args, **kwargs):
        # Calculate the Sub Total Paid Price
        if (self.cashless_adjustment and hasattr(self.cashless_adjustment, 'adjustment')):
            self.sub_paid = -self.cashless_adjustment.adjustment

        super().save(*args, **kwargs)
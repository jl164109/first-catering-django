# Project Imports
from common.viewsets import BaseCreateRetrieveDestroyViewSet, BaseCreateRetrieveUpdateDestroyViewSet

# App Imports
from .models import Cart, CartProduct
from .serializers import CartSerializer, CartProductSerializer

class CartViewSet(BaseCreateRetrieveDestroyViewSet):
    """
    This viewset automatically provides `create`, `retrieve` and `delete` actions.
    """
    lookup_field = 'id'

    queryset = Cart.objects.all()
    serializer_class = CartSerializer

class CartProductViewSet(BaseCreateRetrieveUpdateDestroyViewSet):
    """
    This viewset automatically provides CRUD actions.
    """
    lookup_field = 'id'

    queryset = CartProduct.objects.all()
    serializer_class = CartProductSerializer
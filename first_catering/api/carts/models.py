# Project Imports
from common.models import BaseDateTimeModel
from django.db import models

# App Imports
from api.products.models import Product

# Product Model Declaration - Extends BaseDateTime Model
class Cart(BaseDateTimeModel):
    # Model Properties
    total_due = models.DecimalField(verbose_name='Total Due', max_digits=6, decimal_places=2, default=0.00)

    # Readabilty
    class Meta:
        verbose_name = 'Cart'
        verbose_name_plural = 'Carts'

    def __str__(self):
        return '{id}: £{total_due}'.format(
            id = self.id,
            total_due = self.total_due,
        )

# CartProduct Model Declaration - Extends BaseDateTime Model
class CartProduct(BaseDateTimeModel):
    # Foreign Keys
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    cart = models.ForeignKey(Cart, on_delete=models.SET_NULL, null=True)

    # Model Properties
    quantity = models.IntegerField(verbose_name='Quantity', default=0)
    sub_due = models.DecimalField(verbose_name='Sub Total Due', max_digits=6, decimal_places=2, default=0.00)

    # Readabilty
    class Meta:
        verbose_name = 'Cart Product'
        verbose_name_plural = 'Cart Products'

    def __str__(self):
        return '{product}: {quantity} at £{sub_due}'.format(
            # Check if the Product has been set before using the property
            product = 
                self.product.name 
                    if (self.product and hasattr(self.product, 'name'))
                    else 'Product',

            quantity = self.quantity,
            sub_due = self.sub_due,
        )

    # Save Hook
    def save(self, *args, **kwargs):
        # Calculate the Sub Total Due Price
        if (self.product and hasattr(self.product, 'price')):
            self.sub_due = self.quantity * self.product.price

        super().save(*args, **kwargs)
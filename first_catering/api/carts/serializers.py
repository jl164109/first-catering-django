# Project Imports
from common.serializers import BaseDateTimeSerializer

# App Imports
from .models import Cart, CartProduct

class CartSerializer(BaseDateTimeSerializer):
    class Meta:
        model = Cart
        fields = (
            'id',
            'total_due',
            ) + BaseDateTimeSerializer.Meta.fields

class CartProductSerializer(BaseDateTimeSerializer):
    class Meta:
        model = CartProduct
        fields = (
            'id',
            'product',
            'cart',
            'quantity',
            'sub_due',
        ) + BaseDateTimeSerializer.Meta.fields

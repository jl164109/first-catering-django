# Project Imports
from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver
from decimal import Decimal

# App Imports
from .models import CartProduct

@receiver(pre_save, sender=CartProduct)
def cart_product_pre_save(sender, instance, **kwargs):
    instance.pre_change = { 'sub_due': Decimal(0.00), }

    # Store the current value of the instance's sub_due
    if(instance.id):
        instance.pre_change['sub_due'] = sender.objects.get(id=instance.id).sub_due

    # Check for a change in the sub_due and update total_due
    if(instance.pre_change['sub_due'] != instance.sub_due):
        cart_product_change(
            instance,
            difference = (
                instance.sub_due - instance.pre_change['sub_due']
            )
        )

@receiver(pre_delete, sender=CartProduct)
def cart_product_pre_delete(sender, instance, **kwargs):
    # Update total_due to reflect change of removing the sub_due
    cart_product_change(
        instance=instance,
        difference=-(instance.sub_due)
    )

def cart_product_change(instance, difference):
    # Add Difference and Save
    instance.cart.total_due = instance.cart.total_due + difference
    instance.cart.save()

    # Logging
    print(
        'Cart: {cart} Total Due has changed by + £{difference}'.format(
            cart = instance.cart,
            difference = difference
        ).replace('+ £-', '- £')
    )
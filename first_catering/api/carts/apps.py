from django.apps import AppConfig


class CartsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api.carts'
    verbose_name = 'Carts'

    def ready(self):
        from . import receivers
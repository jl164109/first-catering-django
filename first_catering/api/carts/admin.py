# Project Imports
from django.contrib import admin
from common.admin import BaseDateTimeAdmin

# App Imports
from .models import Cart, CartProduct

# Cart Admin Declaration - Extends BaseDateTime Admin
class CartAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Cart Info', {
            'fields': [
                'total_due',
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# CartProduct Admin Declaration - Extends BaseDateTime Admin
class CartProductAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Cart Product Info', {
            'fields': [
                'quantity',
                'sub_due',
            ],
        }),
        ('Cart Product Relationships', {
            'fields': [
                'product',
                'cart',
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# Register Cart Admin to the Admin Dashboard
admin.site.register(Cart, CartAdmin)

# Register Cart Product Admin to the Admin Dashboard
admin.site.register(CartProduct, CartProductAdmin)
# Project Imports
from common.models import BaseDateTimeModel
from django.db import models

# Product Model Declaration - Extends BaseDateTime Model
class Product(BaseDateTimeModel):
    # Model Properties
    name = models.CharField(verbose_name='Name', max_length=50)
    stock = models.IntegerField(verbose_name='Stock', default=0)
    price = models.DecimalField(verbose_name='Price', max_digits=6, decimal_places=2, default=0.00)

    # Readabilty
    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return '{product}: {stock} at £{price} each'.format(
            product = self.name,
            stock = self.stock,
            price = self.price,
        )
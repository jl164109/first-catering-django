# Project Imports
from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver

# App Imports
from api.carts.models import CartProduct

@receiver(pre_save, sender=CartProduct)
def cart_product_pre_save(sender, instance, **kwargs):
    instance.pre_change = { 'quantity': 0, }

    # Store the current value of the instance's quantity
    if(instance.id):
        instance.pre_change['quantity'] = sender.objects.get(id=instance.id).quantity

    # Check for a change in the quantity and update stock
    if(instance.pre_change['quantity'] != instance.quantity):
        cart_product_change(
            instance,
            difference = (
                instance.quantity - instance.pre_change['quantity']
            )
        )

@receiver(pre_delete, sender=CartProduct)
def cart_product_pre_delete(sender, instance, **kwargs):
    # Update stock to reflect change of removing the quantity
    cart_product_change(
        instance=instance,
        difference=-(instance.quantity)
    )

def cart_product_change(instance, difference):
    # Subtract Difference and Save
    instance.product.stock = instance.product.stock - difference
    instance.product.save()

    # Logging
    print(
        'Product: {product} Stock has changed by {difference}'.format(
            product = instance.product,
            difference = -difference
        )
    )
# Project Imports
from django.contrib import admin
from common.admin import BaseDateTimeAdmin

# App Imports
from .models import Product

# Product Admin Declaration - Extends BaseDateTime Admin
class ProductAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Product Info', {
            'fields': [
                'name',
                'stock',
                'price',
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# Register Product Admin to the Admin Dashboard
admin.site.register(Product, ProductAdmin)
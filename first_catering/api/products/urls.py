# Project Imports
from django.urls import path, include
from rest_framework.routers import SimpleRouter

# App Imports
from .viewsets import ProductViewSet

# Initialize SimpleRouter
router = SimpleRouter()

# Base Route Registration
router.register(r'products', ProductViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]
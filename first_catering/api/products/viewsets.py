# Project Imports
from common.viewsets import BaseListRetrieveViewSet

# App Imports
from .models import Product
from .serializers import ProductSerializer

class ProductViewSet(BaseListRetrieveViewSet):
    """
    This viewset automatically provides `list` and `retrieve` actions.
    """
    lookup_field = 'id'

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
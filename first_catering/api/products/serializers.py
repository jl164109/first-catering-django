# Project Imports
from common.serializers import BaseDateTimeSerializer

# App Imports
from .models import Product

class ProductSerializer(BaseDateTimeSerializer):
    class Meta:
        model = Product
        fields = ('id','name','stock','price') + BaseDateTimeSerializer.Meta.fields

# Project Imports
from common.models import BaseDateTimeModel, BaseUUIDModel
from django.db import models

# App Imports
from api.carts.models import Cart
from api.transactions.models import Transaction

# Order Model Declaration - Extends BaseDateTime and Base UUID Models
class Order(BaseDateTimeModel, BaseUUIDModel):
    # Foreign Keys
    cart = models.OneToOneField(Cart, on_delete=models.SET_NULL, null=True)
    transaction = models.OneToOneField(Transaction, to_field='uuid', on_delete=models.SET_NULL, null=True)

    # Model Properties
    closed = models.BooleanField('Order Closed', default=False)

    # Readabilty
    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

    def __str__(self):
        if(not self.closed):
            return '{uuid}: £{total_paid}/£{total_due}'.format(
                uuid = self.uuid,

                # Check if the Transaction has been set before using the property
                total_paid = 
                    self.transaction.total_paid 
                        if (self.transaction and hasattr(self.transaction, 'total_paid'))
                        else '0.00',

                # Check if the Cart has been set before using the property
                total_due = 
                    self.cart.total_due 
                        if (self.cart and hasattr(self.cart, 'total_due'))
                        else '0.00',
            )
        return '{uuid}: Order Closed'.format(
            uuid = self.uuid
        )
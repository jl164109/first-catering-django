# Project Imports
from django.contrib import admin
from common.admin import BaseDateTimeAdmin

# App Imports
from .models import Order

# Order Admin Declaration - Extends BaseDateTime Admin
class OrderAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Order Info', {
            'fields': [
                'closed',
            ],
        }),
        ('Order Relationships', {
            'fields': [
                'cart',
                'transaction',
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# Register Order Admin to the Admin Dashboard
admin.site.register(Order, OrderAdmin)
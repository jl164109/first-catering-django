# Project Imports
from common.serializers import BaseDateTimeSerializer, BaseUUIDSerializer

# App Imports
from .models import Order

class OrderSerializer(BaseDateTimeSerializer, BaseUUIDSerializer):
    class Meta:
        model = Order
        fields = (
            'cart',
            'transaction',
            'closed',
        ) + BaseDateTimeSerializer.Meta.fields + BaseUUIDSerializer.Meta.fields

# Project Imports
from django.urls import path, include
from rest_framework.routers import SimpleRouter

# App Imports
from .viewsets import OrderViewSet

# Initialize SimpleRouter
router = SimpleRouter()

# Base Route Registration
router.register(r'orders', OrderViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]
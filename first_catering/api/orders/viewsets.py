# Project Imports
from common.viewsets import BaseCreateRetrieveUpdateDestroyViewSet

# App Imports
from .models import Order
from .serializers import OrderSerializer

class OrderViewSet(BaseCreateRetrieveUpdateDestroyViewSet):
    """
    This viewset automatically provides CRUD actions.
    """
    lookup_field = 'uuid'

    queryset = Order.objects.all()
    serializer_class = OrderSerializer

# Project Imports
from common.serializers import BaseDateTimeSerializer, BaseUUIDSerializer

# App Imports
from .models import Cashless, CashlessAdjustment

class CashlessSerializer(BaseDateTimeSerializer):
    class Meta:
        model = Cashless
        fields = ('customer', 'balance') + BaseDateTimeSerializer.Meta.fields + BaseUUIDSerializer.Meta.fields

class CashlessAdjustmentSerializer(BaseDateTimeSerializer):
    class Meta:
        model = CashlessAdjustment
        fields = ('cashless', 'adjustment') + BaseDateTimeSerializer.Meta.fields + BaseUUIDSerializer.Meta.fields
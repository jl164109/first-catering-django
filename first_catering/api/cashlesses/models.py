# Project Imports
from common.models import BaseDateTimeModel, BaseUUIDModel
from django.db import models

# App Imports
from api.customers.models import Customer

# Cashless Model Declaration - Extends BaseDateTime and Base UUID Models
class Cashless(BaseDateTimeModel, BaseUUIDModel):
    # Foreign Keys
    customer = models.OneToOneField(Customer, to_field='uuid', on_delete=models.SET_NULL, null=True)

    # Model Properties
    balance = models.DecimalField(verbose_name='Balance', max_digits=6, decimal_places=2, default=0.00)

    # Readabilty
    class Meta:
        verbose_name = 'Cashless'
        verbose_name_plural = 'Cashlesses'

    def __str__(self):
        return '{customer}: £{balance}'.format(
            # Check if the Customer has been set before using the property
            customer = 
                self.customer.name 
                    if (self.customer and hasattr(self.customer, 'name'))
                    else 'Customer',

            balance = self.balance,
        )

# CashlessAdjustment Model Declaration - Extens BaseDateTime and Base UUID Models
class CashlessAdjustment(BaseDateTimeModel, BaseUUIDModel):
    # Foreign Keys
    cashless = models.ForeignKey(Cashless, to_field='uuid', on_delete=models.SET_NULL, null=True)

    # Model Properties
    adjustment = models.DecimalField(verbose_name='Adjustment', max_digits=6, decimal_places=2, default=0.00)

    # Readabilty
    class Meta:
        verbose_name = 'Cashless Adjustment'
        verbose_name_plural = 'Cashless Adjustments'

    def __str__(self):
        # Check if the Cashless and Customer has been set before using the properties
        if(
            (self.cashless and hasattr(self.cashless, 'customer')) and 
            (self.cashless.customer and hasattr(self.cashless.customer, 'name'))
        ):
            return '{customer}: + £{adjustment}'.format(
                customer = self.cashless.customer.name ,
                adjustment = self.adjustment,
            ).replace('+ £-', '- £')

        # Fallback Output
        return 'Customer: {adjustment}'.format(
            adjustment = self.adjustment,
        ).replace('+ £-', '- £')

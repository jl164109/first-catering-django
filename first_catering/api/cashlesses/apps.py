from django.apps import AppConfig


class CashlessesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api.cashlesses'

    def ready(self):
        from . import receivers
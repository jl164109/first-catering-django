# Project Imports
from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver
from decimal import Decimal

# App Imports
from .models import CashlessAdjustment

@receiver(pre_save, sender=CashlessAdjustment)
def cashless_adjustment_pre_save(sender, instance, **kwargs):
    instance.pre_change = { 'adjustment': Decimal(0.00), }

    # Store the current value of the instance's quantity
    if(instance.id):
        instance.pre_change['adjustment'] = sender.objects.get(id=instance.id).adjustment

    # Check for a change in the quantity and update stock
    if(instance.pre_change['adjustment'] != instance.adjustment):
        cashless_adjustment_change(
            instance,
            difference = (
                instance.adjustment - instance.pre_change['adjustment']
            )
        )

@receiver(pre_delete, sender=CashlessAdjustment)
def cashless_adjustment_pre_delete(sender, instance, **kwargs):
    # Update stock to reflect change of removing the quantity
    cashless_adjustment_change(
        instance=instance,
        difference=-(instance.adjustment)
    )

def cashless_adjustment_change(instance, difference):
    # Add Difference and Save
    instance.cashless.balance = instance.cashless.balance + difference
    instance.cashless.save()

    # Logging
    print(
        'Cashless: {cashless} Balance has changed by + £{difference}'.format(
            cashless = instance.cashless,
            difference = difference
        ).replace('+ £-', '- £')
    )
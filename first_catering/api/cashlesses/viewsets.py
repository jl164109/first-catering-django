# Project Imports
from common.viewsets import BaseFilteredListRetrieveViewSet, BaseCreateRetrieveDestroyViewSet

# App Imports
from .models import Cashless, CashlessAdjustment
from .serializers import CashlessSerializer, CashlessAdjustmentSerializer

class CashlessViewSet(BaseFilteredListRetrieveViewSet):
    """
    This viewset automatically provides `list` and `retrieve` actions.
    """
    lookup_field = 'uuid'

    filter_lists = [
        [
            'customer',
        ],
    ]

    queryset = Cashless.objects.all()
    serializer_class = CashlessSerializer

class CashlessAdjustmentViewSet(BaseCreateRetrieveDestroyViewSet):
    """
    This viewset automatically provides `create`, `retrieve` and `destroy` actions.
    """
    lookup_field = 'uuid'

    queryset = CashlessAdjustment.objects.all()
    serializer_class = CashlessAdjustmentSerializer
# Project Imports
from django.urls import path, include
from rest_framework.routers import SimpleRouter

# App Imports
from .viewsets import CashlessViewSet, CashlessAdjustmentViewSet

# Initialize SimpleRouter
router = SimpleRouter()

# Base Route Registration
router.register(r'cashlesses', CashlessViewSet)
router.register(r'cashless-adjustments', CashlessAdjustmentViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]
# Project Imports
from django.contrib import admin
from common.admin import BaseDateTimeAdmin

# App Imports
from .models import Cashless, CashlessAdjustment

# Cashless Admin Declaration - Extends BaseDateTime Admin
class CashlessAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Cashless Info', {
            'fields': [
                'balance',
            ],
        }),
        ('Cashless Relationships', {
            'fields': [
                'customer',
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# CashlessAdjustment Admin Declaration - Extends BaseDateTime Admin
class CashlessAdjustmentAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Cashless Adjustment Info', {
            'fields': [
                'adjustment',
            ],
        }),
        ('Cashless Adjustment Relationships', {
            'fields': [
                'cashless',
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# Register Cashless Admin to the Admin Dashboard
admin.site.register(Cashless, CashlessAdmin)

# Register Cashless Admin to the Admin Dashboard
admin.site.register(CashlessAdjustment, CashlessAdjustmentAdmin)
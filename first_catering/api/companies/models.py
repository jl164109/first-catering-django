# Project Imports
from common.models import BaseDateTimeModel, BaseUUIDModel
from django.db import models

# Company Model Declaration - Extends BaseDateTime and Base UUID Models
class Company(BaseDateTimeModel, BaseUUIDModel):
    # Model Properties
    name = models.CharField(verbose_name='Company Name', max_length=50)

    # Readabilty
    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'

    def __str__(self):
        return '{company}'.format(
            company = self.name,
        )
# Project Imports
from django.contrib import admin
from common.admin import BaseDateTimeAdmin

# App Imports
from .models import Company

# Company Admin Declaration - Extends BaseDateTime Admin
class CompanyAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Company Info', {
            'fields': [
                'name',
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# Register Company Admin to the Admin Dashboard
admin.site.register(Company, CompanyAdmin)
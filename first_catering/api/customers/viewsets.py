# Project Imports
from common.viewsets import BaseFilteredListRetrieveViewSet

# App Imports
from .models import Customer
from .serializers import CustomerSerializer

class CustomerViewSet(BaseFilteredListRetrieveViewSet):
    """
    This viewset automatically provides `retrieve` and `update` actions.
    """
    lookup_field = 'uuid'

    filter_lists = [
        [
            'name',
            'email',
            'mobile_number',
            'pin',
        ],
        [
            'card_number',
        ]
    ]

    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
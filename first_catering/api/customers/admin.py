# Project Imports
from django.contrib import admin
from common.admin import BaseDateTimeAdmin

# App Imports
from .models import Customer

# Customer Admin Declaration - Extends BaseDateTime Admin
class CustomerAdmin(BaseDateTimeAdmin):
    fieldsets = [
        ('Customer Info', {
            'fields': [
                'name',
                'email',
                'mobile_number',
                'card_number',
                'pin',
            ],
        }),
        ('Customer Relationships', {
            'fields': [
                'company',
            ],
        }),
    ] + BaseDateTimeAdmin.fieldsets

# Register Customer Admin to the Admin Dashboard
admin.site.register(Customer, CustomerAdmin)
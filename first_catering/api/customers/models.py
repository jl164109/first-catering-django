# Project Imports
from common.models import BaseDateTimeModel, BaseUUIDModel
from django.db import models
from encrypted_fields.fields import SearchField, EncryptedIntegerField, EncryptedCharField

from django.conf import settings

# App Imports
from api.companies.models import Company

# Customer Model Declaration - Extends BaseDateTime and Base UUID Models
class Customer(BaseDateTimeModel, BaseUUIDModel):
    # Foreign Keys
    company = models.ForeignKey(Company, to_field='uuid', on_delete=models.SET_NULL, null=True)

    # Model Properties
    name = models.CharField(verbose_name='Full Name', max_length=50)
    email = models.EmailField(verbose_name='Email Address', max_length=50)
    mobile_number = models.CharField(verbose_name='Mobile Number', max_length=16)

    # Encrypted Properties (https://pypi.org/project/django-searchable-encrypted-fields/)
    _card_number_data = EncryptedCharField(verbose_name='Card Number', max_length=16)
    card_number = SearchField(hash_key=settings.FIELD_ENCRYPTION_KEYS[0], encrypted_field_name='_card_number_data')

    _pin_data = EncryptedIntegerField(verbose_name='PIN')
    pin = SearchField(hash_key=settings.FIELD_ENCRYPTION_KEYS[0], encrypted_field_name='_pin_data')

    # Readability
    class Meta:
        verbose_name = 'Customer'
        verbose_name_plural = 'Customers'

    def __str__(self):
        return '{company}: {customer}'.format(
            # Check if the Company has been set before using the property
            company = 
                self.company.name 
                    if (self.company and hasattr(self.company, 'name'))
                    else 'Company',

            customer = self.name,
        )
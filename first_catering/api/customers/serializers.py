# Project Imports
from common.serializers import BaseDateTimeSerializer, BaseUUIDSerializer

# App Imports
from .models import Customer

class CustomerSerializer(BaseDateTimeSerializer, BaseUUIDSerializer):
    class Meta:
        model = Customer
        fields = (
            'company',
            'name',
            'email',
            'mobile_number',
            'card_number',
        ) + BaseDateTimeSerializer.Meta.fields + BaseUUIDSerializer.Meta.fields

# Project Imports
from rest_framework import serializers

# App Imports
from .models import BaseDateTimeModel, BaseUUIDModel

# Abstract Serializer for Created and Updated DateTimes
# Optional serializer used with most endpoints
class BaseDateTimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseDateTimeModel
        fields = ('created_datetime', 'updated_datetime',)
        abstract = True

# Abstract Serializer for UUID Fields
# Used with serializer used when a model utilizes a UUID
class BaseUUIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseUUIDModel
        fields = ('uuid',)
        abstract = True

# Project Imports
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin, ListModelMixin
from rest_framework.permissions import IsAuthenticated 
from django.conf import settings

# App Imports
from .mixins import FilteredListModelMixin, ParameteredCreateModelMixin

class BaseListRetrieveViewSet(
        ListModelMixin,
        RetrieveModelMixin,
        GenericViewSet
    ):

    """
    A viewset that provides `list` and `retrieve` actions.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.

    Requires token-based user authentication for access
    """

    if(not settings.DEBUG):
        permission_classes = (IsAuthenticated,) 

class BaseRetrieveViewSet(
        RetrieveModelMixin,
        GenericViewSet
    ):

    """
    A viewset that provides a `retrieve` action.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.

    Requires token-based user authentication for access
    """

    if(not settings.DEBUG):
        permission_classes = (IsAuthenticated,) 

class BaseCreateRetrieveUpdateViewSet(
        CreateModelMixin,
        RetrieveModelMixin,
        UpdateModelMixin,
        GenericViewSet
    ):

    """
    A viewset that provides `create`, `retrieve` and `update` actions.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.

    Requires token-based user authentication for access
    """

    if(not settings.DEBUG):
        permission_classes = (IsAuthenticated,)

class BaseCreateRetrieveDestroyViewSet(
        CreateModelMixin,
        RetrieveModelMixin,
        DestroyModelMixin,
        GenericViewSet
    ):

    """
    A viewset that provides `create`, `retrieve` and `destroy` actions.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.

    Requires token-based user authentication for access
    """

    if(not settings.DEBUG):
        permission_classes = (IsAuthenticated,) 

class BaseCreateRetrieveUpdateDestroyViewSet(
        CreateModelMixin,
        RetrieveModelMixin,
        UpdateModelMixin,
        DestroyModelMixin,
        GenericViewSet
    ):

    """
    A viewset that provides CRUD actions.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.

    Requires token-based user authentication for access
    """

    if(not settings.DEBUG):
        permission_classes = (IsAuthenticated,) 

class BaseFilteredListRetrieveViewSet(
        FilteredListModelMixin,
        RetrieveModelMixin,
        GenericViewSet
    ):

    """
    A viewset that provides `filtered-list` and `retrieve` actions.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.

    `filter-list` requires a `filter_lists` array attribute to function

    Requires token-based user authentication for access
    """

    if(not settings.DEBUG):
        permission_classes = (IsAuthenticated,) 

class BaseParameteredCreateRetrieveUpdateViewSet(
        ParameteredCreateModelMixin,
        RetrieveModelMixin,
        UpdateModelMixin,
        GenericViewSet
    ):

    """
    A viewset that provides `parametered-create`, `retrieve` and `update` actions.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.

    `parameter-list` requires a `parameter_lists` array attribute to function

    Requires token-based user authentication for access
    """

    if(not settings.DEBUG):
        permission_classes = (IsAuthenticated,) 
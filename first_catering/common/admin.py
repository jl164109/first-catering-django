from django.contrib import admin

class BaseDateTimeAdmin(admin.ModelAdmin):
    readonly_fields = (
        'created_datetime',
        'updated_datetime',
    )

    fieldsets = [
        ('Date Logging', {
            'fields': [
                'created_datetime',
                'updated_datetime',
            ],
            'classes': [
                'collapse'
            ],
        }),
    ]
from django.db import models
import uuid

# Abstract Model for Created & Updated DateTime Fields.
# These fields are standard for most model and are useful for keeping a good log of field changes
# The created is set when the field is first added and the updated is set upon save
class BaseDateTimeModel(models.Model):
    created_datetime = models.DateTimeField(verbose_name='Created DateTime', auto_now_add=True)
    updated_datetime = models.DateTimeField(verbose_name='Updated DateTime', auto_now=True)

    class Meta:
        abstract = True

# Abstract Model for UUID as a Unqiue Key.
# A UUID Key is safer than an auto-increment foramt
# While I could set the PK to being the UUID Field this is slower for internal querying
# As long as the PK isn't externally queryable you can use a psudeo-uuid field for external querying
# There is an extreamly low chance of anyone guessing a UUID
class BaseUUIDModel(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    class Meta:
        abstract = True
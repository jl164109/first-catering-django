# Project Imports
from django.http.response import Http404
from rest_framework.status import HTTP_201_CREATED
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

class CustomFieldLookupMixin:
    """
    Apply this mixin to do a custom lookup
    """

    def get_object(self):
        # Get and Filter Queryset
        queryset = self.filter_queryset(self.get_queryset())

        # Useful Self Fields
        print(self.lookup_field)
        print(self.kwargs)

        # Try to Find the Object
        try:    
            found_object = queryset.get(name='test')

        except:
            raise Http404

        # Check if the requester has permission to retrieve the object
        self.check_object_permissions(self.request, found_object)
        return found_object

def check_request_data_meets_requirements(requirements_lists, request):
    # Check Applied Filters VS Required Filters
        found_requirements_lists = False
        for requirements_list in requirements_lists:
            # When we find that a list doesn't match
            # Mark a Boolean as False and BREAK
            # To indicate that this cycle is incorrect
            # Otherwise let the FOR finish naturally
            # Without changing the Boolean from True
            found_requirements = {}
            found_requirements_list = True
            for requirement in requirements_list:
                if requirement not in request.data:
                    print('Failed to match')
                    found_requirements_list = False
                    break

                found_requirements[requirement] = request.data[requirement]

            # When the list we found has finished it's cycle
            # Indicate that one of the lists finished successfully
            # Mark a Boolean as True and BREAK
            # Otherwise let the FOR finish naturally
            # Without changing the Boolean from False
            if (found_requirements_list):
                found_requirements_lists = True
                break

        # Throw Missing Filters
        if(not found_requirements_lists):
            raise ValidationError

        # Return Requirements Mapped
        return found_requirements

class FilteredListModelMixin:
    """
    List a queryset with enforced filters
    """
    def list(self, request, *args, **kwargs):
        # Get and Filter Queryset
        queryset = self.filter_queryset(self.get_queryset())

        # Check Applied Filters VS Required Filters
        found_filters = check_request_data_meets_requirements(self.filter_lists, request)

        # Try to Find the Object
        try:
            found_object = queryset.get(**found_filters)

        except:
            raise Http404

        # Check if the requester has permission to retrieve the object
        self.check_object_permissions(self.request, found_object)

        # Serialize and return found object
        serializer = self.get_serializer(found_object, many=False)
        return Response(serializer.data)

class ParameteredCreateModelMixin:
    """
    Create an object with customized serializer data
    """
    def construct_serializer_data(request, parameters):
        return request.data

    def create(self, request, *args, **kwargs):
        # Check Applied Parameters VS Required Parameters
        found_parameters = check_request_data_meets_requirements(self.parameter_lists, request)

        # Get and Validate Serializer using Hook Data
        serializer = self.get_serializer(data=self.construct_serializer_data(request=request, parameters=found_parameters))
        serializer.is_valid(raise_exception=True)

        # Utilize serialized data
        self.perform_create(serializer)

        # Set headers and respond
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save()

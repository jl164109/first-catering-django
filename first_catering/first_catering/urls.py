"""first_catering URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from api.products.urls import urlpatterns as products_urlpatterns
from api.cashlesses.urls import urlpatterns as cashlesses_urlpatterns
from api.customers.urls import urlpatterns as customers_urlpatterns
from api.transactions.urls import urlpatterns as transactions_urlpatterns
from api.carts.urls import urlpatterns as carts_urlpatterns
from api.orders.urls import urlpatterns as orders_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
] + products_urlpatterns + cashlesses_urlpatterns + customers_urlpatterns + transactions_urlpatterns + carts_urlpatterns + orders_urlpatterns

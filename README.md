# First Catering API

The First Catering API is a Django Project which manages client data and that exposes integrated Django REST API Endpoints.

The API allows for cashless-catering till systems to create and process orders.
These orders can relate to a cart of multiple items and to a payment.
The payments can relate to an integrated cashless system tied to each customer.

The Django System keeps track of adjustments made to a cashless account and allows for account addition as well as subtraction.
The Django System also maintains running sub-totals and product-stock whilst an order is in process.

## Requirements Installation

Use the Python package manager [pip](https://pip.pypa.io/en/stable/) to [pip-install](https://pip.pypa.io/en/stable/cli/pip_install/) all of the required packages
```
pip install -r requirements.txt
```

When installing additional required packages it's best to add them to the requirements file for other developers
```
pip freeze > requirements.txt
```

Following requirement installation you can [cd](https://en.wikipedia.org/wiki/Cd_(command)) into the `first_catering` project folder.

## Source Control

All new features to be implemented on a `feature` branch.
Critical bug fixes to be implemented on a `hotfix` branch.
Non-Critical bug fixes to be implemented on a `bugfix` branch.
Further package requirement changes to be implmented on a `release` branch.
Going forwards, once integrated into a system, please use `deployment` branches for staging and production deployments.

## Running Server

With Django running the server for Database and API Connections is rather straight forwards.
Simply run the following command from the project folder.
```
python manage.py runserver
```

## Making Model Changes

Before making a model change it's best to prepare for what happens for new and existing data:
 - Updating all references to a field to reflect changes.
 - Setting either a one-time default or a permanent default to aid data entry.
 - Keeping existing models/fields and marking them as legacy.

After your change has been confirmed you can [make a migration](https://docs.djangoproject.com/en/3.2/ref/django-admin/#makemigrations) that will be ran upon `deployment`
```
python manage.py makemigrations
```

Following this you can [apply your migration](https://docs.djangoproject.com/en/3.2/ref/django-admin/#migrate) to your development system
```
python manage.py migrate
```

## Data Inspection

Whilst testing has been performed to reduce the amount of bugs you may still need to manually inspect data.
The best way to do this is to activate an [interactive shell](https://docs.djangoproject.com/en/3.2/ref/django-admin/#shell).
```
python manage.py shell
```

You can also enter a database shell for SQL execution.
It's recommended that you try to resolve the issue using the Django Shell instead.